This project is trying to show how to use UI Ext.NET 
that allow you to write Ext.JS UI with your favorite C# code.


* Pull source code
* Open project with Visual Studio 2012
* Download SQLite database file from [this link](https://docs.google.com/file/d/0BzUe8Xcray60Q0RTQ2d1M29KY0U/edit)  and unzip then put in C: root folder.

Screen shot of application

![2014-08-26_1651.png](https://bitbucket.org/repo/ERrMgr/images/2896751874-2014-08-26_1651.png)

![menu2.png](https://bitbucket.org/repo/ERrMgr/images/2363139072-menu2.png)

![plan.png](https://bitbucket.org/repo/ERrMgr/images/440846564-plan.png)

![form.png](https://bitbucket.org/repo/ERrMgr/images/562793580-form.png)

![form2.png](https://bitbucket.org/repo/ERrMgr/images/1498739788-form2.png)

![list.png](https://bitbucket.org/repo/ERrMgr/images/257045473-list.png)

![admin.png](https://bitbucket.org/repo/ERrMgr/images/820830024-admin.png)