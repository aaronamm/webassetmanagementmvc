using System.Collections.Generic;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Controllers
{
    public class AddEmployeeViewModel
    {
        public IList<Position> Positions { get; set; }
        public object[] Genders { get; set; }
    }
}