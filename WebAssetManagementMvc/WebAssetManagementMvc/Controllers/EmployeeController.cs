﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using NHibernate.Linq;
using WebAssetManagementMvc.Messages.Reponses;
using WebAssetManagementMvc.Models;
using log4net;
using Position = WebAssetManagementMvc.Models.Position;

namespace WebAssetManagementMvc.Controllers
{
    public class EmployeeController : Controller
    {
        private ILog _log = LogManager.GetLogger(typeof(EmployeeController).Name);
        private const int NOTIFY_DELAY = 1000;//ms

        public Ext.Net.MVC.PartialViewResult EmployeeList(string containerId)
        {

            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);

            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);

            try
            {
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Loaded",
                                     Html = "Employee list loaded",
                                     AutoHide = true,
                                     HideDelay = NOTIFY_DELAY,
                                 }).Show();


                using (var session = SessionFactory.GetNewSession())
                {

                    var employeeListResponse = session.Query<Employee>()
                        .Select(e => new EmployeeListResponse()
                     {
                         Address = e.Address,
                         EmployeeId = e.EmployeeId,
                         EmployeeName = e.EmployeeName,
                         Password = e.Password,
                         PositionId = e.Position.PositionId,
                         PositionName = e.Position.PositionName,
                         Sex = e.Sex,
                         Username = e.Username,
                     }).ToList();

                    view.Model = employeeListResponse;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }

            return view;
        }

        [HttpPost]
        public DirectResult Edit(int id, string field, string oldValue, string newValue, [Bind(Prefix = "object")] Employee employee)
        {
            _log.InfoFormat("employee name: {0}", employee.EmployeeName);
            _log.InfoFormat("id: {0}", id);
            var message = "<b>Property:</b> {0}<br /><b>Field:</b> {1}<br /><b>Old Value:</b> {2}<br /><b>New Value:</b> {3}";
            using (var session = SessionFactory.GetNewSession())
            {

                var employeeFromDb = session.Get<Employee>(id);
                var type = typeof(Employee);

                //set new value
                var propertyToChange = type.GetProperty(field);
                var newValueCorrectType = Convert.ChangeType(newValue, propertyToChange.PropertyType);
                propertyToChange.SetValue(employeeFromDb, newValueCorrectType, null);


                session.Flush();
            }

            // Send Message...
            X.Msg.Notify(new NotificationConfig()
            {
                Title = "Edit Record #" + id.ToString(),
                Html = string.Format(message, id, field, oldValue, newValue),
                Width = 250
            }).Show();

            X.GetCmp<Store>("employeeStore").GetById(id).Commit();

            return this.Direct();
        }

        [HttpGet]
        public Ext.Net.MVC.PartialViewResult AddEmployee(string containerId)
        {
            X.Msg.Notify(new NotificationConfig
                             {
                                 Icon = Icon.Accept,
                                 Title = "Loaded",
                                 Html = "AddEmployee form loaded",
                                 AutoHide = true,
                                 HideDelay = NOTIFY_DELAY,
                             }).Show();

            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            AddEmployeeViewModel viewModel;
            using (var session = SessionFactory.GetNewSession())
            {
                viewModel = new AddEmployeeViewModel()
                {
                    Genders = new object[] { new[] { "M" }, new[] { "F" } },
                    Positions = session.Query<Position>().ToList()
                };
            }
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo)
            {
                Model = viewModel
            };
            return view;
        }//end method

        [HttpPost]
        public DirectResult AddEmployee(Employee employee)
        {

            try
            {
                using (var session = SessionFactory.GetNewSession())
                {
                    session.Save(employee);
                    session.Flush();
                }

                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Saved successfully",
                                     Html = string.Format("Saved new employee id: {0}", employee.EmployeeId),
                                     AutoHide = true,
                                 }).Show();

            }
            catch (Exception ex)
            {

                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error saving new employee",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }

            return this.Direct();
        }

        [HttpDelete]
        public DirectResult DeleteEmployee(int employeeId)
        {
            try
            {

                using (var session = SessionFactory.GetNewSession())
                {
                    session.Delete(new Employee() { EmployeeId = employeeId });
                    session.Flush();
                }
                var store = X.GetCmp<Store>("employeeStore");
                var record = store.GetById(employeeId);
                store.Remove(record);

                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Delete successfully",
                                     Html = string.Format("Deleted employee id: {0} from database", employeeId),
                                     AutoHide = true,
                                 }).Show();

            }
            catch (Exception ex)
            {

                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error deleting  employee",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }

            return this.Direct();
        }
    }
}
