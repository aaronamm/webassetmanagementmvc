﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAssetManagementMvc.Helpers;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Controllers
{
    public class MainController : Controller
    {
        //
        // GET: /Admin/

        [HttpAuthorize]
        public ActionResult Index()
        {

            var userId = (int)HttpContext.Items["userId"];

            using (var session = SessionFactory.GetNewSession())
            {
                var user = session.Get<Employee>(userId);
                ViewBag.PositionId = user.Position.PositionId;
            }


            return View();
        }

    }
}
