namespace WebAssetManagementMvc.Controllers
{
    public class ShowPlanCheckListReportRecordViewModel
    {

        //public virtual int AssetGroupId { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public bool IsAllDay { get; set; }

        public string EmployeeName { get; set; }
        public string LocationName { get; set; }
        public string StatusName { get; set; }
        public int StatusId { get; set; }
        public string ZoneName { get; set; }

        public string AssetName { get; set; }
        public int AssetId { get; set; }

        public string AreaName { get; set; }

        public string MaterialName { get; set; }

    }
}