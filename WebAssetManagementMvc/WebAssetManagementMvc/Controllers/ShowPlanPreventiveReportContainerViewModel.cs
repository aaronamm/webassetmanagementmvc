using System.Collections.Generic;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Controllers
{
    public class ShowPlanPreventiveReportContainerViewModel
    {
        public IList<ShowPlanPreventiveReportRecordViewModel>[] PlanPreventives { get; set; }
        public string[] AssetNames { get; set; }
    }

    public class ShowPlanCheckListReportContainerViewModel
    {
        public IList<ShowPlanCheckListReportRecordViewModel>[] CheckLists { get; set; }
        public string[] AssetNames { get; set; }
        public IList<Status> Statuses { get; set; }
    }
}