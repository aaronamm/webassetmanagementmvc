﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using NHibernate.Linq;
using WebAssetManagementMvc.Helpers;
using WebAssetManagementMvc.Models;
using log4net;

namespace WebAssetManagementMvc.Controllers
{
    public class UserController : Controller
    {

        private ILog _log = LogManager.GetLogger(typeof(UserController).Name);

        public ActionResult LogIn()
        {
            return View();

        }

        [HttpPost]
        public ActionResult LogInUser(string username, string password)
        {
            using (var session = SessionFactory.GetNewSession())
            {
                var employee = session.Query<Employee>()
                    .SingleOrDefault(e => e.Username == username && e.Password == password);

                if (employee != null)
                {
                    X.AddScript(string.Format("location.href='{0}';",Url.Action("Index","Main")));

                    //log in user

                    var cookie = new HttpCookie(HttpAuthorizeAttribute.COOKIE_KEY)
                    {
                        Value = employee.EmployeeId.ToString(),//userId
                        Expires = DateTime.UtcNow.AddMinutes(10)
                    };

                    if (Response.Cookies[HttpAuthorizeAttribute.COOKIE_KEY] != null)
                    {
                        Response.SetCookie(cookie);
                        _log.DebugFormat("update cookie {0}", cookie.Name);
                    }
                    else
                    {
                        Response.Cookies.Add(cookie);
                        _log.DebugFormat("add cookie {0}", cookie.Name);
                    }

                }
                else
                {
                    var alert = X.Msg.Alert("LogIn Error", "Wrong username or password");
                    alert.Show();
                }


                return this.Direct();
            }


        }
        public ActionResult LogOut()
        {

                    var cookie = new HttpCookie(HttpAuthorizeAttribute.COOKIE_KEY)
                    {
                        Value = true.ToString(),
                        Expires = DateTime.UtcNow.AddMinutes(-10)//set to the past
                    };

                    if (Response.Cookies[HttpAuthorizeAttribute.COOKIE_KEY] != null)
                    {
                        Response.SetCookie(cookie);
                        _log.DebugFormat("update cookie {0} to expire", cookie.Name);
                    }
                    else
                    {
                        Response.Cookies.Add(cookie);
                        _log.DebugFormat("add cookie {0} to expire", cookie.Name);
                    }

                    return RedirectToAction("LogIn");
        }

        public ActionResult AjaxLogIn()
        {
            return View();
        }
    }
}
