﻿using System.Web.Mvc;
using System.Web.Routing;
using WebAssetManagementMvc.App_Start;
using log4net.Config;

namespace WebAssetManagementMvc
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }


        protected void Application_Start()
        {
            XmlConfigurator.Configure();


            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);

            RouteConfig.RegisterRoutes(RouteTable.Routes);


            SessionFactory.Init();
        }
    }
}