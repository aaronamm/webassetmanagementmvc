﻿using System.ComponentModel;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace WebAssetManagementMvc.Helpers
{
    public class CustomJsonResult : ActionResult
    {
        private readonly object _obj;

        public CustomJsonResult(object obj)
        {
            _obj = obj;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new DateTimeConverter("yyyy-MM-ddTHH:mm:ss"));
            var json = JsonConvert.SerializeObject(_obj, Formatting.Indented, settings);

            response.Clear();
            response.ContentType = "application/json; charset=utf-8";

            response.Write(json);
            response.End();
        }
    }
}