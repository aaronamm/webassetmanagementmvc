﻿using System;
using System.Globalization;

namespace WebAssetManagementMvc.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime? ToDate(this string dateTimeStr, string dateFmt)
        {
            // example: var dt="2011-03-21 13:26".toDate("yyyy-MM-dd HH:mm");
            const DateTimeStyles style = DateTimeStyles.AllowWhiteSpaces;
            DateTime? result = null;
            DateTime dt;
            if(!string.IsNullOrEmpty(dateTimeStr))
            {
                dateTimeStr = dateTimeStr.Replace("\"", "");
            }

            if (DateTime.TryParseExact(dateTimeStr, dateFmt,
                CultureInfo.InvariantCulture, style, out dt)) result = dt;
            return result;
        }

        public static bool ToBoolean(this string boolString)
        {
            bool result;
            if (bool.TryParse(boolString.ToLower(), out result))
            {
                return result; 
            }
            return false;
        }
    }
}