﻿using System;
using System.Configuration;
using System.Web.Mvc;
using log4net;

namespace WebAssetManagementMvc.Helpers
{
    public class HttpAuthorizeAttribute : AuthorizeAttribute
    {
        private ILog _log = LogManager.GetLogger(typeof(HttpAuthorizeAttribute).Name);

        public const string COOKIE_KEY = "HttpAuthorizeVaildate";

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var loggedInUrl = ConfigurationManager.AppSettings["LoggedInUrl"];
            var request = filterContext.HttpContext.Request;
            var response = filterContext.HttpContext.Response;

            try
            {

                if (string.IsNullOrEmpty(loggedInUrl))
                {
                    throw new InvalidOperationException(string.Format("no LoggedInUrl value in AppSettings"));
                }
                //get cookie
                var cookie = request.Cookies[COOKIE_KEY];
                if (cookie == null)
                {
                    response.Redirect(loggedInUrl);
                    _log.DebugFormat("no cookie: {0}", COOKIE_KEY);
                    return;
                }

                //var setting = cookie.Values;
                var userId = 0;
                int.TryParse( cookie.Value,out userId);
                if (userId==0) 
                {
                    response.Redirect(loggedInUrl);
                    _log.DebugFormat("userId value: {0} is invalid", userId);
                    return;
                }

                //add more ten minutes logged in
                cookie.Expires = DateTime.UtcNow.AddMinutes(10);
                response.SetCookie(cookie);
                _log.DebugFormat("update Expires of cookie: {0} to {1}", cookie.Name, cookie.Expires);
                filterContext.HttpContext.Items["userId"] = userId;

            }
            catch (InvalidOperationException ex)
            {
                _log.Error(ex);
                response.Clear();
                response.StatusCode = 500;
                response.Write(ex.Message);
                response.End();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                response.Redirect(loggedInUrl);
            }
        }

    }
}