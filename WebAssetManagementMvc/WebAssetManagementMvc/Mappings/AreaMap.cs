﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class AreaMap:ClassMap<Area>
    {
        public AreaMap() {

        Table("AM_AREA");

        Id(x => x.AreaId).Column("AREA_ID").GeneratedBy.Assigned();

        Map(x => x.LocationId).Column("LOCATION_ID");     
        Map(x => x.ZoneId).Column("ZONE_ID");
        Map(x => x.Detail).Column("DETAIL");

        
        }

    }
}