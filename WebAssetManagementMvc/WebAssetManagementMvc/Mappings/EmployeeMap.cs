﻿using FluentNHibernate.Mapping;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class EmployeeMap:ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Table("AM_EMPLOYEE");

            Id(x => x.EmployeeId).Column("EMPLOYEE_ID").GeneratedBy.Identity();
            References(x => x.Position).ForeignKey("none").Column("POSITION_ID");//.Not.Nullable();
            
            Map(x => x.EmployeeName).Column("EMPLOYEE_NAME");
            Map(x => x.Address).Column("ADDRESS");
            Map(x => x.Sex).Column("SEX");
            Map(x => x.Username).Column("USERNAME");
            Map(x => x.Password).Column("PASSWORD");
        }
    }
}