﻿using FluentNHibernate.Mapping;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class NotifyBreakDownMap : ClassMap<NotifyBreakDown>
    {
        public NotifyBreakDownMap()
        {

            Table("AM_NOTIFY_BD");

            Id(x => x.Id).Column("ID").GeneratedBy.Identity();
            Map(x => x.Title).Column("TITLE");

            References(x => x.Material).ForeignKey("none").Column("MATERIAL_ID");
            References(x => x.Asset).ForeignKey("none").Column("ASSET_ID");

            References(x => x.Zone).ForeignKey("none").Column("ZONE_ID");
            References(x => x.Location).ForeignKey("none").Column("LOCATION_ID");
            References(x => x.Area).ForeignKey("none").Column("AREA_ID");

            References(x => x.EmployeeInformant).ForeignKey("none").Column("EMP_INFORMANT");
            References(x => x.EmployeeBearer).ForeignKey("none").Column("EMP_BEARER");
            References(x => x.Status).ForeignKey("none").Column("STATUS");

            Map(x => x.SymptomBefore).Column("SYMPTOM_BEFORE");
            Map(x => x.SymptomAfter).Column("SYMPTOM_AFTER");

            Map(x => x.NotifyDate).Column("DATE_NOTIFY");
            Map(x => x.MaintenanceDate).Column("DATE_MAINTENANCE");

            Map(x => x.FormState).Column("FORM_STATE");

        }//end method

    }//end class

}