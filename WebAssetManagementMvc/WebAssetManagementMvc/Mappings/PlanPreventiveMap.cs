﻿using FluentNHibernate.Mapping;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{

    public class PlanPreventiveMap:ClassMap<PlanPreventive>
    {
        //ID TITLE, BEGIN_DATE, END_DATE, IS_ALL_DATE, ZONE_ID, LACATION_ID, AREA_ID, MATERIA_ID, EMPLOYEE_ID,STATUS

        public PlanPreventiveMap()
        { 
            Table("AM_PLAN_PREVENTIVE");

            Id(x => x.Id).Column("ID").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.Title).Column("TITLE").Not.Nullable();
            Map(x => x.Detail).Column("DETAIL").Nullable();

            Map(x => x.BeginDate).Column("BEGIN_DATE").Not.Nullable();
            Map(x => x.EndDate).Column("END_DATE").Nullable();
            Map(x => x.IsAllDay).Column("IS_ALL_DAY").Nullable().Default("0");

            

           
            References(x => x.Employee).ForeignKey("none").Column("EMPLOYEE_ID");//.Not.Nullable();
            References(x => x.Location).ForeignKey("none").Column("LOCATION_ID");//.Not.Nullable();
            References(x => x.Status).ForeignKey("none").Column("STATUS_ID");//.Not.Nullable();

            References(x => x.Asset).ForeignKey("none").Column("ASSET_ID");//.Not.Nullable();
            References(x => x.Zone).ForeignKey("none").Column("ZONE_ID");//.Not.Nullable();
            References(x => x.Material).ForeignKey("none").Column("MATERIAL_ID");//.Not.Nullable();
            References(x => x.Area).ForeignKey("none").Column("AREA_ID");//.Not.Nullable();

            Map(x => x.FormState).Column("FORM_STATE");

        }






    }
}
//ID
//NO
//SONE_ID
//LOCATION_ID
//AREA_ID
//ASSET_ID
//ASSET_GROUP_ID
//BEGIN_DATE
//DETAIL
//END_DATE
//IS_ALL_DAY
//TITLE
