﻿using FluentNHibernate.Mapping;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class PositionMap : ClassMap<Position>
    {
        public PositionMap()
        {
            Table("AM_POSITION");

            Id(x => x.PositionId,"POSITION_ID").GeneratedBy.Identity();
            Map(x => x.PositionName,"POSITION_NAME");
            Map(x => x.Salary,"SALARY");

            HasMany(x => x.Employees).ForeignKeyConstraintName("none").KeyColumn("POSITION_ID");
        }
    }
}