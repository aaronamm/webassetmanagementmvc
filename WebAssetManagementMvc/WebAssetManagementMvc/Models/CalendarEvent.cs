﻿using System;

namespace WebAssetManagementMvc.Models
{
    public class CalendarEvent
    {
        public string id { get; set; }
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime? end { get; set; }
        public bool allDay { get; set; }
        public string type { get; set; }
        public int rowId { get; set; }

    }
}

