﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class Location
    {
        public virtual int LocationId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Detail { get; set; }
    }
}