﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class Material
    {
        public virtual string MaterialId { get; set; }
        public virtual int AssetGroupId { get; set; }
    }
}