﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Reports
{
    public partial class PmAirMonth1 : System.Web.UI.Page
    {
        private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        private ILog _log = LogManager.GetLogger(typeof(PmAirMonth1).Name);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) //start save
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                _log.DebugFormat("rowId: {0}", rowId);

                using (var session = SessionFactory.GetNewSession())
                {

                    var pmAirMonth1 = session.Get<PlanPreventive>(rowId); //มาจากฐานข้อมูลชื่อPlanPreventive
                    if (pmAirMonth1.FormState != null)
                    {
                        var state = JsonConvert.DeserializeObject<dynamic>(pmAirMonth1.FormState);
                        _log.DebugFormat("loaded FormStat string: {0}", state);

                        txtRecordDetail.Text = state.RecordDetail;

                        txtDetail1.Text = state.Detail1;
                        txtDetail2.Text = state.Detail2;
                        txtDetail3.Text = state.Detail3;
                        txtDetail4.Text = state.Detail4;
                        txtDetail5.Text = state.Detail5;
                        txtDetail6.Text = state.Detail6;
                        txtDetail7.Text = state.Detail7;
                        txtDetail8.Text = state.Detail8;
                       
                        cbRagular1.Checked = state.Ragular1;
                        cbRagular2.Checked = state.Ragular2;
                        cbRagular3.Checked = state.Ragular3;
                        cbRagular4.Checked = state.Ragular4;
                        cbRagular5.Checked = state.Ragular5;
                        cbRagular6.Checked = state.Ragular6;
                        cbRagular7.Checked = state.Ragular7;
                        cbRagular8.Checked = state.Ragular8;
                        
                        cbRepair1.Checked = state.Repair1;
                        cbRepair2.Checked = state.Repair2;
                        cbRepair3.Checked = state.Repair3;
                        cbRepair4.Checked = state.Repair4;
                        cbRepair5.Checked = state.Repair5;
                        cbRepair6.Checked = state.Repair6;
                        cbRepair7.Checked = state.Repair7;
                        cbRepair8.Checked = state.Repair8;
                        
                        cbChange1.Checked = state.Change1;
                        cbChange2.Checked = state.Change2;
                        cbChange3.Checked = state.Change3;
                        cbChange4.Checked = state.Change4;
                        cbChange5.Checked = state.Change5;
                        cbChange6.Checked = state.Change6;
                        cbChange7.Checked = state.Change7;
                        cbChange8.Checked = state.Change8;
                       
                        txtNotation1.Text = state.Notation1;
                        txtNotation2.Text = state.Notation2;
                        txtNotation3.Text = state.Notation3;
                        txtNotation4.Text = state.Notation4;
                        txtNotation5.Text = state.Notation5;
                        txtNotation6.Text = state.Notation6;
                        txtNotation7.Text = state.Notation7;
                        txtNotation8.Text = state.Notation8;
                        

                    }
                    lblId.Text = pmAirMonth1.Id.ToString();
                    lblBuilder.Text = "ตึก ict มหาวิทยาลัยพะเยา";
                    lblArea.Text = pmAirMonth1.Area.AreaId;
                    lblLocation.Text = pmAirMonth1.Location.Name;
                    lblParamMaterial.Text = pmAirMonth1.Material.MaterialId;
                    lblZone.Text = pmAirMonth1.Zone.ZoneDetail;
                    //lblAsset.Text = ChecklistAirWeek1.Asset.AssetName;
                    lblEmployee.Text = pmAirMonth1.Employee.EmployeeName;
                    lblBeginDate.Text = pmAirMonth1.BeginDate.ToString();
                    lblEndDate.Text = pmAirMonth1.EndDate.ToString();
                    lblIsAllDate.Text = pmAirMonth1.IsAllDay.ToString();
                }

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            dynamic state = new ExpandoObject();

            state.RecordDetail = txtRecordDetail.Text;

            state.Detail1 = txtDetail1.Text;
            state.Detail2 = txtDetail2.Text;
            state.Detail3 = txtDetail3.Text;
            state.Detail4 = txtDetail4.Text;
            state.Detail5 = txtDetail5.Text;
            state.Detail6 = txtDetail6.Text;
            state.Detail7 = txtDetail7.Text;
            state.Detail8 = txtDetail8.Text;
           
            state.Ragular1 = cbRagular1.Checked;
            state.Ragular2 = cbRagular2.Checked;
            state.Ragular3 = cbRagular3.Checked;
            state.Ragular4 = cbRagular4.Checked;
            state.Ragular5 = cbRagular5.Checked;
            state.Ragular6 = cbRagular6.Checked;
            state.Ragular7 = cbRagular7.Checked;
            state.Ragular8 = cbRagular8.Checked;
           
            state.Repair1 = cbRepair1.Checked;
            state.Repair2 = cbRepair2.Checked;
            state.Repair3 = cbRepair3.Checked;
            state.Repair4 = cbRepair4.Checked;
            state.Repair5 = cbRepair5.Checked;
            state.Repair6 = cbRepair6.Checked;
            state.Repair7 = cbRepair7.Checked;
            state.Repair8 = cbRepair8.Checked;
           
            state.Change1 = cbChange1.Checked;
            state.Change2 = cbChange2.Checked;
            state.Change3 = cbChange3.Checked;
            state.Change4 = cbChange4.Checked;
            state.Change5 = cbChange5.Checked;
            state.Change6 = cbChange6.Checked;
            state.Change7 = cbChange7.Checked;
            state.Change8 = cbChange8.Checked;
           
            state.Notation1 = txtNotation1.Text;
            state.Notation2 = txtNotation2.Text;
            state.Notation3 = txtNotation3.Text;
            state.Notation4 = txtNotation4.Text;
            state.Notation5 = txtNotation5.Text;
            state.Notation6 = txtNotation6.Text;
            state.Notation7 = txtNotation7.Text;
            state.Notation8 = txtNotation8.Text;
           
            var formState = JsonConvert.SerializeObject(state, Formatting.Indented);
            _log.DebugFormat("new formState : {0}", formState);

            using (var session = SessionFactory.GetNewSession())
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                var pmAirMonth1 = session.Get<PlanPreventive>(rowId);
                pmAirMonth1.FormState = formState;
                session.Flush();
                _log.DebugFormat("saved id: {0}", rowId);
            }

        }//end method
    }
}