﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PmElevator12Month1.aspx.cs" Inherits="WebAssetManagementMvc.Reports.PmElevator12Month1" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 140px;
        }
        #frmHead {
            height: 1096px;
            margin-top: 0px;
            margin-bottom: 0px;
            background-color: #FFFFFF;
        }
        .auto-style9 {
            width: 166px;
        }
        .auto-style16 {
            height: 31px;
            width: 82px;
        }
        .auto-style18 {
            width: 166px;
            height: 31px;
        }
        .auto-style23 {
            width: 98px;
            height: 29px;
        }
        .auto-style26 {
            width: 166px;
            height: 29px;
        }
        .auto-style27 {
            text-align: right;
            width: 82px;
        }
        .auto-style29 {
            height: 29px;
            text-align: right;
            width: 82px;
        }
        .auto-style31 {
            height: 31px;
            text-align: right;
            width: 107px;
        }
        .auto-style33 {
            height: 29px;
            text-align: right;
            width: 107px;
        }
        .auto-style36 {
            height: 31px;
            width: 98px;
        }
        .auto-style42 {
            font-weight: bold;
            text-decoration: underline;
        }
        .auto-style43 {
            font-weight: bold;
        }
        .auto-style45 {
            width: 77px;
        }
        .auto-style46 {
            width: 77px;
            height: 29px;
        }
        .auto-style48 {
            height: 31px;
            width: 77px;
        }
        .auto-style49 {
            text-align: right;
            width: 107px;
        }
        #TextArea1 {
            top: 805px;
            left: 142px;
            position: absolute;
            height: 82px;
            width: 726px;
        }
        .auto-style50 {
            width: 98px;
            height: 24px;
        }
        .auto-style51 {
            height: 24px;
            text-align: right;
            width: 107px;
        }
        .auto-style52 {
            width: 77px;
            height: 24px;
        }
        .auto-style53 {
            width: 166px;
            height: 24px;
        }
        .auto-style54 {
            height: 24px;
            text-align: right;
            width: 82px;
        }
        .auto-style55 {
            width: 90%;
            font-size: small;
            font-family: "TH SarabunPSK";
            border: 0px solid #000000;
        }
        .auto-style58 {
            text-align: center;
        }
        .auto-style59 {
            text-align: center;
            width: 100px;
        }
        .auto-style61 {
            text-align: center;
            width: 39px;
            font-size: medium;
        }
        .auto-style67 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 39px;
            height: 19px;
            font-size: medium;
        }
        .auto-style68 {
            text-align: center;
            height: 19px;
        }
        .auto-style70 {
            text-align: center;
            width: 100px;
            height: 19px;
        }
        .newStyle1 {
            background-color: #EAF5F7;
        }
        .auto-style79 {
            text-align: left;
            width: 339px;
            font-size: medium;
        }
        .auto-style82 {
            text-align: left;
            width: 339px;
            height: 19px;
            font-size: medium;
        }
        .auto-style83 {
            text-align: center;
            height: 6px;
            font-size: medium;
        }
        .auto-style84 {
            font-size: x-small;
        }
        .auto-style85 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 39px;
            height: 32px;
            font-size: medium;
        }
        .auto-style87 {
            text-align: left;
            width: 339px;
            height: 32px;
            font-size: medium;
        }
        .auto-style88 {
            text-align: center;
            height: 32px;
        }
        .auto-style89 {
            text-align: center;
            width: 100px;
            height: 32px;
        }
        .auto-style90 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 60px;
            font-size: medium;
        }
        .auto-style91 {
            text-align: center;
            width: 60px;
            height: 32px;
            }
        .auto-style92 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 60px;
            height: 19px;
            font-size: medium;
        }
        .auto-style93 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 39px;
            height: 10px;
            font-size: medium;
        }
        .auto-style94 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 60px;
            height: 10px;
            font-size: medium;
        }
        .auto-style95 {
            text-align: left;
            width: 339px;
            height: 10px;
            font-size: medium;
        }
        .auto-style96 {
            text-align: center;
            height: 10px;
        }
        .auto-style97 {
            text-align: center;
            width: 100px;
            height: 10px;
        }
        .auto-style98 {
            font-size: small;
        }
        .auto-style99 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 39px;
            font-size: medium;
        }
        .auto-style100 {
            font-size: medium;
        }
        .auto-style101 {
            width: 98px;
        }
        .auto-style102 {
            text-align: center;
            width: 100px;
            font-size: medium;
        }
        .auto-style103 {
            text-align: center;
            font-size: medium;
        }
        .auto-style104 {
            text-align: center;
            width: 339px;
            font-size: medium;
        }
        .auto-style105 {
            text-align: center;
            width: 60px;
            font-size: medium;
        }
        .auto-style106 {
            text-align: center;
            height: 6px;
            font-size: medium;
            width: 41px;
        }
        .auto-style107 {
            text-align: center;
            width: 41px;
            height: 32px;
        }
        .auto-style108 {
            text-align: center;
            width: 41px;
        }
        .auto-style109 {
            text-align: center;
            width: 41px;
            height: 10px;
        }
        .auto-style110 {
            text-align: center;
            width: 41px;
            height: 19px;
        }
        .auto-style111 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 39px;
            height: 21px;
            font-size: medium;
        }
        .auto-style112 {
            border: 1px solid #C0C0C0;
            text-align: center;
            width: 60px;
            height: 21px;
            font-size: medium;
        }
        .auto-style113 {
            text-align: left;
            width: 339px;
            height: 21px;
            font-size: medium;
        }
        .auto-style114 {
            text-align: center;
            height: 21px;
        }
        .auto-style115 {
            text-align: center;
            width: 41px;
            height: 21px;
        }
        .auto-style116 {
            text-align: center;
            width: 100px;
            height: 21px;
        }
        </style>
</head>
<body style="border: 1px solid black; width:1000px;height:1100px; margin-bottom: 0px; font-size: small; font-family: 'TH SarabunPSK';">
    <form id="frmHead" runat="server">
        
        <asp:Panel ID="Panel1" runat="server" Height="24px" style="text-align: center; margin-top: 32px" CssClass="auto-style98">
            <asp:Label ID="Label14" runat="server" Text="ช่วงเวลา :" style="top: 48px; left: 18px; position: absolute; height: 19px; width: 76px; font-weight: 700; font-family: 'TH SarabunPSK'; text-align: left;" CssClass="auto-style100"></asp:Label>
            <asp:Label ID="Label15" runat="server" style="font-family: 'TH SarabunPSK'; top: 48px; left: 83px; position: absolute; height: 16px; width: 132px; text-align: right; margin-bottom: 169px;" Text="ประจำ 12 เดือน" CssClass="auto-style100"></asp:Label>
            <asp:Label ID="lblNameProject" runat="server" CssClass="auto-style42" Font-Names="TH SarabunPSK" Font-Size="Small" style="font-size: medium; top: 48px; left: 14px; position: absolute; height: 19px; width: 995px;" Text="ระบบบริหารอุปกรณ์ประกอบอาคาร"></asp:Label>
            <asp:Label ID="Label16" runat="server" style="top: 79px; left: 127px; position: absolute; height: 16px; width: 89px; text-align: right; font-family: 'TH SarabunPSK';" Text="ลิฟท์" CssClass="auto-style100"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="Panel3" runat="server" Font-Names="TH SarabunPSK" Height="24px" CssClass="auto-style98">
            <asp:Label ID="lblAsset" runat="server" Text="ชนิดอุปกรณ์ :" style="top: 79px; left: 14px; position: absolute; height: 26px; width: 110px; font-weight: 700;" Font-Names="TH SarabunPSK" CssClass="auto-style100"></asp:Label>
            <asp:Label ID="lblNameForm" runat="server" Font-Names="TH SarabunPSK" style="font-size: medium; top: 76px; left: 14px; position: absolute; height: 17px; width: 995px; text-align: center; bottom: 458px;" Text="ตารางการตรวจสอบและบำรุงรักษา" CssClass="auto-style42"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="pnlParameter" runat="server" style="top: 130px; left: 12px; position: absolute; height: 148px; width: 994px; bottom: 273px;" CssClass="auto-style98">
            <table style="width:75%; top: 1px; left: 125px; position: absolute; height: 141px; font-family: 'TH SarabunPSK'; font-size: medium;">
                <tr>
                    <td class="auto-style36">
                        <asp:Label ID="Label1" runat="server" Text="ใบงานเลขที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style31">
                        <asp:Label ID="lblId" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style48"></td>
                    <td class="auto-style18">
                    </td>
                    <td class="auto-style16"></td>
                </tr>
                <tr>
                    <td class="auto-style101">
                        <asp:Label ID="Label2" runat="server" Text="อาคาร :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style49">
                        <asp:Label ID="lblBuilder" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style45"></td>
                    <td class="auto-style9">
                        <asp:Label ID="Label6" runat="server" Text="วัสดุ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style27">
                        <asp:Label ID="lblParamMaterial" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style23">
                        <asp:Label ID="Label3" runat="server" Text="โซน :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style33">
                        <asp:Label ID="lblZone" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style46"></td>
                    <td class="auto-style26">
                        <asp:Label ID="Label7" runat="server" Text="เวลาเริ่มงาน :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style29">
                        <asp:Label ID="lblBeginDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style50">
                        <asp:Label ID="Label4" runat="server" Text="สถานที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style51">
                        <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style52"></td>
                    <td class="auto-style53">
                        <asp:Label ID="Label8" runat="server" Text="เวลาสิ้นสุด :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style54">
                        <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style101">
                        <asp:Label ID="Label5" runat="server" Text="พื้นที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style49">
                        <asp:Label ID="lblArea" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style45"></td>
                    <td class="auto-style9">
                        <asp:Label ID="Label9" runat="server" Text="งานตลอดวัน :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style27">
                        <asp:Label ID="lblIsAllDate" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>

        </asp:Panel>

        <asp:Panel ID="Panel2" runat="server" CssClass="auto-style98">

        </asp:Panel>

        <asp:Panel ID="Panel4" runat="server" style="top: 927px; left: 654px; position: absolute; height: 68px; width: 216px; text-align: center; font-size: x-small;" CssClass="auto-style98">
            <br />
            &nbsp;<asp:Label ID="lblEmployee" runat="server" Font-Names="TH SarabunPSK" CssClass="auto-style100"></asp:Label>     
            <br />
            <br />
            <asp:Label ID="Label12" runat="server" Text="พนักงานผู้ตรวจเช็ค" Font-Names="TH SarabunPSK" CssClass="auto-style100"></asp:Label>
        </asp:Panel>

           
        
        
        
        <div style="top: 291px; left: 140px; position: absolute; height: 19px; width: 738px">
            <asp:Label ID="Label13" runat="server" style="font-family: 'TH SarabunPSK'; font-weight: 700; font-size: medium; text-decoration: underline;" Text="PREVENTIVE MAINTENANCE ORDER" Font-Size="Small"></asp:Label>
        </div>
        <div style="top: 317px; left: 9px; position: absolute; height: 423px; width: 1001px">
            <table align="center" cellspacing="0" class="auto-style55" style="border: 0px solid #000000; width: 74%;">
                <tr class="auto-style84">
                    <td class="auto-style61" rowspan="2" style="border: 1px solid #C0C0C0;">เลขที่        <td class="auto-style105" rowspan="2" style="border: 1px solid #C0C0C0;">ข้อกำหนด</td>
                    <td class="auto-style104" rowspan="2" style="border: 1px solid #C0C0C0;">รายละเอียด</td>
                    <td class="auto-style103" rowspan="2" style="border: 1px solid #C0C0C0;">รายละเอียด การบำรุงรักษา</td>
                    <td class="auto-style103" colspan="3" style="border: 1px solid #C0C0C0;">ข้อตรวจพบ/พิจารณา</td>
                    <td class="auto-style102" rowspan="2" style="border: 1px solid #C0C0C0;">หมายเหตุ</td>
                </tr>
                <tr class="auto-style84">
                    <td class="auto-style83" style="border: 1px solid #C0C0C0;">ปกติ</td>
                    <td class="auto-style83" style="border: 1px solid #C0C0C0;">ซ่อม</td>
                    <td class="auto-style106" style="border: 1px solid #C0C0C0;">เปลี่ยน</td>
                </tr>
                <tr>
                    <td class="auto-style85">1</td>
                    <td class="auto-style91" style="border: 1px solid #C0C0C0;"><span class="auto-style100">ปกติ</td>
                    <td class="auto-style87" style="border: 1px solid #C0C0C0;">ตรวจเช็คการทางานของโอเวอร์โหลดรีเลย์ </span> </td>
                    <td class="auto-style88" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail1"></asp:TextBox></td>
                    <td class="auto-style88" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular1" runat="server" Text=" " />
                    </td>
                    <td class="auto-style88" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair1" runat="server" Text=" " />
                    </td>
                    <td class="auto-style107" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange1" runat="server" Text=" " />
                    </td>
                    <td class="auto-style89" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style99">2</td>
                    <td class="auto-style90">ปกติ </td>
                    <td class="auto-style79" style="border: 1px solid #C0C0C0;">ตรวจเช็คสกรูจุดต่อสาย/ระดับแรงดันไฟฟ้าภายในตู้คอนโทรลไฟฟ้า</span></td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail2"></asp:TextBox></td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular2" runat="server" Text=" " />
                    </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair2" runat="server" Text=" " />
                    </td>
                    <td class="auto-style108" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange2" runat="server" Text=" " />
                    </td>
                    <td class="auto-style59" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation2"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style93">3</td>
                    <td class="auto-style94">สะอาด </td>
                    <td class="auto-style95" style="border: 1px solid #C0C0C0;">ตรวจทำความสะอาดรางตัวลิฟต์ และรางตุ้มน้าหนัก</span></td>
                    <td class="auto-style96" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail3"></asp:TextBox></td>
                    <td class="auto-style96" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular3" runat="server" Text=" " />
                    </td>
                    <td class="auto-style96" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair3" runat="server" Text=" " />
                    </td>
                    <td class="auto-style109" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange3" runat="server" Text=" " />
                    </td>
                    <td class="auto-style97" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style67">4&nbsp;</td>
                    <td class="auto-style92">ปกติ </td>
                    <td class="auto-style82" style="border: 1px solid #C0C0C0;">ตรวจเช็คทำความสะอาดรอกขับ </span> </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail4"></asp:TextBox></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular4" runat="server" Text=" " />
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair4" runat="server" Text=" " />
                    </td>
                    <td class="auto-style110" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange4" runat="server" Text=" " />
                    </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation4"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style67">5</td>
                    <td class="auto-style92">ปกติ </td>
                    <td class="auto-style82" style="border: 1px solid #C0C0C0;">ตรวจเช็คขนาดเส้นผ่าศูนย์กลางของลวดสลึงขับลิฟต์</span></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail5"></asp:TextBox></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular5" runat="server" Text=" " />
                     </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair5" runat="server" Text=" " />
                     </td>
                    <td class="auto-style110" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange5" runat="server" Text=" " />
                     </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation5"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style67">6</td>
                    <td class="auto-style92">ปกติ </td>
                    <td class="auto-style82" style="border: 1px solid #C0C0C0;">ตรวจเช็คระดับน้ามันเกียร์ และเปลี่ยนถ่ายตามระยะเวลาที่ กำหนด</span></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail6"></asp:TextBox></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular6" runat="server" Text=" " />
                     </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair6" runat="server" Text=" " />
                     </td>
                    <td class="auto-style110" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange6" runat="server" Text=" " />
                     </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation6"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style67">7</td>
                    <td class="auto-style92">แน่น </td>
                    <td class="auto-style82" style="border: 1px solid #C0C0C0;">ขันตรวจความแน่นของน๊อตยึดต่าง ๆ </span> </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail7"></asp:TextBox></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular7" runat="server" Text=" " />
                     </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair7" runat="server" Text=" " />
                     </td>
                    <td class="auto-style110" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange7" runat="server" Text=" " />
                     </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation7"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style111">8</td>
                    <td class="auto-style112">ปกติ</td>
                    <td class="auto-style113" style="border: 1px solid #C0C0C0;">ตรวจเช็คมอเตอร์พัดลมระบายความร้อน และปริมาณแรงลม</span></td>
                    <td class="auto-style114" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail8"></asp:TextBox></td>
                    <td class="auto-style114" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular8" runat="server" Text=" " />
                     </td>
                    <td class="auto-style114" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair8" runat="server" Text=" " />
                     </td>
                    <td class="auto-style115" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange8" runat="server" Text=" " />
                     </td>
                    <td class="auto-style116" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation8"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style67">9
                        <div style="top: 439px; left: 131px; position: absolute; height: 23px; width: 867px; text-align: left;">
                            บันทึกการตรวจสอบ/ซ่อมบำรุง
                            <asp:TextBox runat="server" ID="txtRecordDetail" style="top: 49px; left: 3px; position: absolute; height: 76px; width: 731px; text-align: left"></asp:TextBox>
                        </div>
                     </td>
                    <td class="auto-style92">สะอาด</td>
                    <td class="auto-style82" style="border: 1px solid #C0C0C0;">ถอดทำความสะอาดฟิวส์/ เซอร์กิตเบรดเกอร์</span></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtDetail9"></asp:TextBox></td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular9" runat="server" Text=" " />
                     </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRepair9" runat="server" Text=" " />
                     </td>
                    <td class="auto-style110" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange9" runat="server" Text=" " />
                     </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox runat="server" ID="txtNotation9"></asp:TextBox></td>
                </tr>
                </table>

           
        
        
        
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="top: 703px; left: 800px; position: absolute; height: 26px; width: 56px" Text="บันทึก" />

           
        
        
        
        </div>

           
        
        
        
    </form>
</body>
</html>
