﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Reports
{
    public partial class PmElevator6Month11 : System.Web.UI.Page
    {
         private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";
         private ILog _log = LogManager.GetLogger(typeof(PmElevator6Month11).Name);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) //start save
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                _log.DebugFormat("rowId: {0}", rowId);

                using (var session = SessionFactory.GetNewSession())
                {

                    var pmElevator6Month11 = session.Get<PlanPreventive>(rowId); //มาจากฐานข้อมูลชื่อPlanPreventive
                    if (pmElevator6Month11.FormState != null)
                    {
                        var state = JsonConvert.DeserializeObject<dynamic>(pmElevator6Month11.FormState);
                        _log.DebugFormat("loaded FormStat string: {0}", state);

                        txtRecordDetail.Text = state.RecordDetail;

                        txtDetail1.Text = state.Detail1;
                        txtDetail2.Text = state.Detail2;
                        txtDetail3.Text = state.Detail3;
                        txtDetail4.Text = state.Detail4;
                        txtDetail5.Text = state.Detail5;
                        txtDetail6.Text = state.Detail6;
                        txtDetail7.Text = state.Detail7;

                        cbRagular1.Checked = state.Ragular1;
                        cbRagular2.Checked = state.Ragular2;
                        cbRagular3.Checked = state.Ragular3;
                        cbRagular4.Checked = state.Ragular4;
                        cbRagular5.Checked = state.Ragular5;
                        cbRagular6.Checked = state.Ragular6;
                        cbRagular7.Checked = state.Ragular7;

                        cbRepair1.Checked = state.Repair1;
                        cbRepair2.Checked = state.Repair2;
                        cbRepair3.Checked = state.Repair3;
                        cbRepair4.Checked = state.Repair4;
                        cbRepair5.Checked = state.Repair5;
                        cbRepair6.Checked = state.Repair6;
                        cbRepair7.Checked = state.Repair7;

                        cbChange1.Checked = state.Change1;
                        cbChange2.Checked = state.Change2;
                        cbChange3.Checked = state.Change3;
                        cbChange4.Checked = state.Change4;
                        cbChange5.Checked = state.Change5;
                        cbChange6.Checked = state.Change6;
                        cbChange7.Checked = state.Change7;

                        txtNotation1.Text = state.Notation1;
                        txtNotation2.Text = state.Notation2;
                        txtNotation3.Text = state.Notation3;
                        txtNotation4.Text = state.Notation4;
                        txtNotation5.Text = state.Notation5;
                        txtNotation6.Text = state.Notation6;
                        txtNotation7.Text = state.Notation7;
                    }

                    lblId.Text = pmElevator6Month11.Id.ToString();
                    lblBuilder.Text = "ตึก ict มหาวิทยาลัยพะเยา";
                    lblArea.Text = pmElevator6Month11.Area.AreaId;
                    lblLocation.Text = pmElevator6Month11.Location.Name;
                    lblParamMaterial.Text = pmElevator6Month11.Material.MaterialId;
                    lblZone.Text = pmElevator6Month11.Zone.ZoneDetail;
                    //lblAsset.Text = PmElevator6Month.Asset.AssetName;
                    lblEmployee.Text = pmElevator6Month11.Employee.EmployeeName;
                    lblBeginDate.Text = pmElevator6Month11.BeginDate.ToString(DATE_FORMAT);
                    lblEndDate.Text = pmElevator6Month11.EndDate.ToString();
                    lblIsAllDate.Text = pmElevator6Month11.IsAllDay.ToString();
                }

            }
          
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

             dynamic state = new ExpandoObject();

             state.RecordDetail = txtRecordDetail.Text;

             state.Detail1 = txtDetail1.Text;
             state.Detail2 = txtDetail2.Text;
             state.Detail3 = txtDetail3.Text;
             state.Detail4 = txtDetail4.Text;
             state.Detail5 = txtDetail5.Text;
             state.Detail6 = txtDetail6.Text;
             state.Detail7 = txtDetail7.Text;

             state.Ragular1 = cbRagular1.Checked;
             state.Ragular2 = cbRagular2.Checked;
             state.Ragular3 = cbRagular3.Checked;
             state.Ragular4 = cbRagular4.Checked;
             state.Ragular5 = cbRagular5.Checked;
             state.Ragular6 = cbRagular6.Checked;
             state.Ragular7 = cbRagular7.Checked;

             state.Repair1 = cbRepair1.Checked;
             state.Repair2 = cbRepair2.Checked;
             state.Repair3 = cbRepair3.Checked;
             state.Repair4 = cbRepair4.Checked;
             state.Repair5 = cbRepair5.Checked;
             state.Repair6 = cbRepair6.Checked;
             state.Repair7 = cbRepair7.Checked;

             state.Change1 = cbChange1.Checked;
             state.Change2 = cbChange2.Checked;
             state.Change3 = cbChange3.Checked;
             state.Change4 = cbChange4.Checked;
             state.Change5 = cbChange5.Checked;
             state.Change6 = cbChange6.Checked;
             state.Change7 = cbChange7.Checked;

             state.Notation1 = txtNotation1.Text;
             state.Notation2 = txtNotation2.Text;
             state.Notation3 = txtNotation3.Text;
             state.Notation4 = txtNotation4.Text;
             state.Notation5 = txtNotation5.Text;
             state.Notation6 = txtNotation6.Text;
             state.Notation7 = txtNotation7.Text;

            var formState = JsonConvert.SerializeObject(state, Formatting.Indented);
            _log.DebugFormat("new formState : {0}", formState);

            using (var session = SessionFactory.GetNewSession())
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                var pmElevator12Month1 = session.Get<PlanPreventive>(rowId);
                pmElevator12Month1.FormState = formState;
                session.Flush();
                _log.DebugFormat("saved id: {0}", rowId);
            }

        }
    }
}