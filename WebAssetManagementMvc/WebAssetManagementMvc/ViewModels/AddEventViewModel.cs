﻿using System.Collections.Generic;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.ViewModels
{
    public class AddEventViewModel
    {
        public IList<Employee> Employees { get; set; }
        public IList<Location> Locations { get; set; }
        public IList<Asset> Assets { get; set; }
        public IList<Area> Areas { get; set; }
        public IList<Zone> Zones { get; set; }
        public IList<Status> Statuses { get; set; }
        public IList<Material> Materials { get; set; }
    }
}